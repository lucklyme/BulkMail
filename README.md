# BulkMail

#### 介绍
该脚本基于python编写。用于批量发送工资条。邮件发送使用的是exchange。后续会增加smtp发送的方式。



#### 软件架构
python+exchangelib+openpyxl


#### 安装教程

1.  pip install openpyxl
2.  pip install exchangelib
3.  pip install urllib3
4.  pip install configparser

#### 使用说明

1.  依据实际情况调整config.ini中的参数
2.  双击`工资表批量发送.bat`,软件会提示当前配置情况，如确认无误，输入`1`即可执行后续程序。
3.  工资条格式参见模板，第一列必须为姓名，最后一列必须为邮箱，中间随意


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
